package com.example.cassandra.repository

import com.example.cassandra.entity.Book
import org.springframework.data.cassandra.repository.CassandraRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface BookRepository : CassandraRepository<Book, UUID>