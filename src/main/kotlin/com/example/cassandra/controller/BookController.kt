package com.example.cassandra.controller

import com.example.cassandra.entity.Book
import com.example.cassandra.service.BookService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.security.InvalidParameterException
import java.util.*

@Controller
class BookController {

    var logger = LoggerFactory.getLogger(BookController::class.java)

    @Autowired
    lateinit var service: BookService;

    @GetMapping("/list")
    fun list(model: MutableMap<String, Any>): String {
        model.put("list", service.list());
        return "list";
    }

    @GetMapping("edit")
    fun edit(@RequestParam id: UUID?, model: MutableMap<String, Any>): String {

        val b:Book;

        if(id != null) {
            b = service.get(id).orElse(Book(null))
        } else {
            b = Book(null)
        }

        model.put("book", b);
        return "edit";
    }

    @PostMapping("edit")
    fun editPost(@ModelAttribute b:Book, redirectAttributes: RedirectAttributes): String {
        if (b.id == null) {
            b.id = UUID.randomUUID()
        }

        val newBook = service.save(b);
        redirectAttributes.addFlashAttribute("message", "Ajout/Modif du livre ${newBook}")
        return "redirect:/list";
    }

    @GetMapping("remove/{id}")
    fun remove(@PathVariable id:UUID, redirectAttributes: RedirectAttributes): String {
        if (id == null) {
            throw InvalidParameterException("Id is mandatory")
        }

        service.delete(id)
        redirectAttributes.addFlashAttribute("message", "Suppression du livre ID=${id}")
        return "redirect:/list";
    }

}