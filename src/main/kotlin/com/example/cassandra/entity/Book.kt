package com.example.cassandra.entity

import org.springframework.data.cassandra.core.mapping.Column
import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table
import java.util.*

@Table
data class Book(@PrimaryKey var id: UUID?,  @Column var title: String = "")