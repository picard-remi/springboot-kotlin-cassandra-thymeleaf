package com.example.cassandra.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration
import org.springframework.data.cassandra.config.SchemaAction
import org.springframework.data.cassandra.core.cql.keyspace.CreateKeyspaceSpecification
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories
import java.util.*

@Configuration
@EnableCassandraRepositories(basePackages = arrayOf("com.example.cassandra.repository"))
class CassandraConfig : AbstractCassandraConfiguration() {

    @Value("\${cassandra.keyspace}")
    private val keyspace:String = "ks"

    override fun getKeyspaceCreations(): MutableList<CreateKeyspaceSpecification> {
        return Arrays.asList(CreateKeyspaceSpecification.createKeyspace(keyspace).ifNotExists())
    }

    override fun getSchemaAction(): SchemaAction {
        return SchemaAction.CREATE_IF_NOT_EXISTS
    }

    override fun getEntityBasePackages(): Array<String> {
        return arrayOf("com.example.cassandra.entity")
    }

    override fun getKeyspaceName(): String {
        return this.keyspace
    }

    override fun getMetricsEnabled(): Boolean {
        return false
    }


}