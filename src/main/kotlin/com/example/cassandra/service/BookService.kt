package com.example.cassandra.service

import com.example.cassandra.entity.Book
import com.example.cassandra.repository.BookRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class BookService {
    @Autowired
    lateinit var repo: BookRepository;

    fun list(): List<Book> {
        return repo.findAll();
    }

    fun get(id: UUID): Optional<Book> {
        return repo.findById(id);
    }

    fun save(b: Book): Book {
        return repo.save(b);
    }

    fun delete(id: UUID) {
        return repo.deleteById(id)
    }

}