# Spring Boot Spring Data REST With Cassandra

## Install Cassandra

`docker run --name cassandra -d cassandra`

## Install Cassandra Client

`docker run -it --link cassandra:cassandra-server --rm cassandra cqlsh cassandra-server`

```
Connected to Test Cluster at cassandra-server:9042.
[cqlsh 5.0.1 | Cassandra 3.11.3 | CQL spec 3.4.4 | Native protocol v4]
Use HELP for help.
cqlsh>
INSERT INTO ks.book (id, title) VALUES ('75d3bc88-0ef5-474e-925d-7abd0bc167e2', 'Le livre de la jungle');
```

## Run

Run As `com/example/cassandra/CassandraApplication.kt`

Or

`mvn spring-boot:run`